#include "Menu.h"
#include <iostream>
#include <vector>
#include "Shape.h"
#include "Canvas.h"
#include <string>
#include "Point.h"
#include "Circle.h"
#include "Arrow.h"
#include "Rectangle.h"
#include "Triangle.h"
#include <cstdlib>

#define EXIT 3

Menu::Menu() 
{

}

Menu::~Menu()
{
	_shapes.clear();
}

void Menu::controlStartSystemMenu()
{
	int _choice;
	do{
		do {
			std::cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << std::endl;
			std::cin >> _choice;

		} while (_choice >EXIT || _choice < 0);
		switch (_choice) {
		case 0:
			//adds new shape
			controlAddShapeSystemMenu();
			break;
		case 1:
			// modify or get information from a current shape
			controlActionsOnShapeSystemMenu();
			break;
		case 2:
			// delete all of the shapes
			deleteAllShapesSystem();
			break;
		case EXIT:
			_shapes.clear();
			break;
		}
	}while (_choice != EXIT);
}

void Menu::controlAddShapeSystemMenu()
{
	int _choice;

	std::string _name = "";

	int _x, _y; // parameters of a point

	int _radius; // circle



	int _width, _length; // rectangle
	do {
		std::cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << std::endl;
		std::cin >> _choice;

	} while (_choice > EXIT || _choice < 0);
	switch (_choice) {
	case 0:
		//adds circle
		std::cout << "Please enter X:" << std::endl;
		std::cin >> _x;

		std::cout << "Please enter Y:" << std::endl;
		std::cin >> _y;

		Point a(_x, _y);

		std::cout << "Please enter radius:" << std::endl;
		std::cin >> _radius;

		std::cout << "Please enter the name of the shape:" << std::endl;
		std::cin >> _name;


		Circle circle(a, _radius, "Circle", _name);

		a.~Point();

		break;
	case 1:
		//adds arrow
		for (int i = 1; i <= 2; i++) {
			std::cout << "Enter the X of point number: " << i << std::endl;
			std::cin >> _x;

			std::cout << "Enter the Y of point number: " << i << std::endl;
			std::cin >> _y;

			switch (i) {
			case 1:
				Point a(_x, _y);
				break;
			case 2:
				Point b(_x, _y);
				break;
			}

		}
		std::cout << "Please enter the name of the shape:" << std::endl;
		std::cin >> _name;

		Arrow arrow(a, b, _radius, "Arrow", _name);

		a.~Point();
		b.~Point();
		break;

	case 2:
		//adds triangle
		for (int i = 1; i <= 3; i++) {
			std::cout << "Enter the X of point number: " << i << std::endl;
			std::cin >> _x;

			std::cout << "Enter the Y of point number: " << i << std::endl;
			std::cin >> _y;

			switch (i) {
			case 1:
				Point a(_x, _y);
				break;
			case 2:
				Point b(_x, _y);
				break;
			case 3:
				Point c(_x, _y);
				break;
			}
		}

		std::cout << "Please enter the name of the shape:" << std::endl;
		std::cin >> _name;

		
		if ((a.getX() == b.getX() && b.getX() == c.getX()) || (a.getY() == b.getY() && b.getY() == c.getY())) {
			std::cout << "The points entered create a line." << std::endl;
			system("PAUSE");
		}
		else {
			Triangle triangle(a, b, c, _radius, "Triangle", _name);
		}

		a.~Point();
		b.~Point();
		c.~Point();


		break;
		

	case 3:
		//adds rectangle
		std::cout << "Enter the X of the to left corner:" << std::endl;
		std::cin >> _x;

		std::cout << "Enter the Y of the top left corner:" << std::endl;
		std::cin >> _y;

		std::cout << "Please enter the length of the shape:" << std::endl;
		std::cin >> _length;

		std::cout << "Please enter the width of the shape:" << std::endl;
		std::cin >> _width;

		Point a(_x, _y);

		std::cout << "Please enter the name of the shape:" << std::endl;
		std::cin >> _name;


		if (_width < 0 || _length < 0) {
			std::cout << "The width or the length you entered in invalid." << std::endl;
			system("PAUSE");
		}
		else {
			Rectangle rectangle(a, _length, _width, "Rectangle", _name);
		}

		a.~Point();
		break;
	}
	
}

void Menu::controlActionsOnShapeSystemMenu()
{
	int _choice, _size, _index;
	_size = _shapes.size();
	do {
		for (int i = 0; i < _size; i++) {
			std::cout << "Enter " << i << " for  " + _shapes[i].getType() << "(" << _shapes[i].getName() << ")" << std::endl;
		}
		std::cin >> _choice;
	} while (_choice > _size || _choice < 0);
	_index = _choice;
	// _shapes[_index];
	std::cout <<  "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape." << std::endl;
	std::cin >> _choice;
	switch (_choice) {
	case 0:
		//move shape
		
		break;
	case 1:
		//get details
		_shapes[_index].printDetails();
		break;
	case 2:
		//remove shape
		_shapes.erase(_index);
		break;
	}

}

void Menu::deleteAllShapesSystem()
{
	int _size;
	_size = _shapes.size();
	for (int i = 0; i < _size; i++) {
		_shapes[i].clearDraw(this->_canvas);
	}
	_shapes.clear();

}


