#include "Circle.h"
#include "Point.h"


Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : 
	Shape(type, name), center(center)
{
	this->center = center;
	this->radius = radius;

}

Circle::~Circle()
{
}

const Point& Circle::getCenter() const
{
	return Point(this->center.getX(), this->center.getY());
}

double Circle::getRadius() const
{
	return this->radius;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

double Circle::getArea()
{
	return PI * pow(this->radius ,2.0);
}

double Circle::getPerimeter()
{
	return 2.0*Pi*this->radius;
}


