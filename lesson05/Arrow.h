#pragma once
#include "Polygon.h"
#include "Point.h"

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
	~Arrow();

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	// override functions if need (virtual + pure virtual)
	virtual double getArea();
	virtual double getPerimeter();


private:
	std::vector<Point> _points;
};