#include "Shape.h"
#include <iostream>

Shape::Shape(const std::string & name, const std::string & type)
{
	this->_name = name;
	this->_type = type;
}
Shape::~Shape()
{
}
void Shape::printDetails() const
{
	std::cout << this->_name << this->_type << this->getArea() << this->getPerimeter() << std::endl;
}
std::string Shape::getType() const
{
	return this->_type;
}
std::string Shape::getName() const
{
	return this->_name;
}
