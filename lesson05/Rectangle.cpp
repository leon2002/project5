#include "Rectangle.h"
#include "Point.h"
#include "Canvas.h"



myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : 
	Polygon(type, name)
{
	this->_points[0] = a;
	this->length = length;
	this->width = width;
	this->_points[1] = Point(a.getX() + width, a.getY() + length);

}

myShapes::Rectangle::~Rectangle()
{
}

double myShapes::Rectangle::getArea()
{
	return (this->width * this->length);
}

double myShapes::Rectangle::getPerimeter()
{
	return (2.0*(this->length + this->width));
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}
