#include "Triangle.h"
#include "Shape.h"


Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : 
	Polygon(type, name)
{
	this->_points[0] = a;
	this->_points[1] = b;
	this->_points[2] = c;


}

Triangle::~Triangle()
{
}

double Triangle::getArea()
{
	return (( _points[0].getX() * (_points[1].getY() - _points[2].getY()))   +    (_points[1].getX() * (_points[2].getY() - _points[0].getY()))   +     (_points[2].getX() * (  _points[0].getY() - _points[1].getY())))/2 ;
}

double Triangle::getPerimeter()
{
	return ( _points[1].distance(_points[2]) + _points[0].distance(_points[1]) + _points[2].distance(_points[0]) );
}



void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

