#include "Polygon.h"
#include "Shape.h"

Polygon::Polygon(const std::string& type, const std::string& name) : 
	Shape(type, name)
{
	this->type = type;
	this->name = name;
}

Polygon::~Polygon()
{

}
