#include "Menu.h"
#include <iostream>
#include <vector>
#include "Shape.h"
#include "Canvas.h"
#include <string>
#include "Point.h"

#define EXIT 3

Menu::Menu() 
{
}

Menu::~Menu()
{
	_shapes.clear();
}

void Menu::controlStartSystemMenu()
{
	int _choice;
	do{
		do {
			std::cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << std::endl;
			std::cin >> _choice;

		} while (_choice >EXIT || _choice < 0);
		switch (_choice) {
		case 0:
			//adds new shape
			controlAddShapeSystemMenu();
			break;
		case 1:
			// modify or get information from a current shape
			controlActionsOnShapeSystemMenu();
			break;
		case 2:
			// delete all of the shapes
			deleteAllShapesSystem();
			break;
		case EXIT:
			_shapes.clear();
			break;
		}
	}while (_choice != EXIT);
}

void Menu::controlAddShapeSystemMenu()
{
	int _choice;
	do {
		std::cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << std::endl;
		std::cin >> _choice;

	} while (_choice > 3 || _choice < 0);
	switch (_choice) {
	case 0:
		//adds circle

		break;
	case 1:
		//adds arrow
		break;
	case 2:
		//adds triangle
		break;
	case 3:
		//adds rectangle
		break;
	}
	
}

void Menu::controlActionsOnShapeSystemMenu()
{
	int _choice, _size, _index;
	_size = _shapes.size();
	do {
		for (int i = 0; i < _size; i++) {
			std::cout << "Enter " << i << " for  " + _shapes[i].getType() << "(" << _shapes[i].getName() << ")" << std::endl;
		}
		std::cin >> _choice;
	} while (_choice > _size || _choice < 0);
	_index = _choice;
	// _shapes[_index];
	std::cout <<  "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape." << std::endl;
	std::cin >> _choice;
	switch (_choice) {
	case 0:
		//move shape
		break;
	case 1:
		//get details
		_shapes[_index].printDetails();
		break;
	case 2:
		//remove shape
		_shapes.erase(_index);
		break;
	}

}

void Menu::deleteAllShapesSystem()
{
	int _size;
	_size = _shapes.size();
	for (int i = 0; i < _size; i++) {
		_shapes[i].clearDraw(this->_canvas);
	}
	_shapes.clear();

}


