#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) :
	Shape(type, name)
{
	this->_points[0] = a;
	this->_points[1] = b;
	

}

Arrow::~Arrow()
{
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

double Arrow::getArea()
{
	return 0.0;
}

double Arrow::getPerimeter()
{
	return _points[0].distance(_points[1]);
}


