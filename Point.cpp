#include "Point.h"
#include <cmath>

Point::Point(double x, double y)
{
	this->x = x;
	this->y = y;
}

Point::Point(const Point& other)
{
	this->x = other.getX();
	this->y = other.getY();

}

Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	return Point(this->x + other.getX(), this->y + other.getY());
}

Point& Point::operator+=(const Point& other)
{
	this->x += other.getX();
	this->y += other.getY();
	return *this;
}

double Point::getX() const
{
	return this->x;
}

double Point::getY() const
{
	return this->y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(this->x - other.getX(), 2.0) + pow(this->y - other.getY(), 2.0));
}


