#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>




class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	void controlStartSystemMenu();
	void controlAddShapeSystemMenu();
	void controlActionsOnShapeSystemMenu();
	void deleteAllShapesSystem();


private: 
	std::vector<Shape> _shapes;
	Canvas _canvas;
};

